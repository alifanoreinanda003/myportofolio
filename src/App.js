import Home from './pages/home';
import "@fontsource/poppins";
import './App.css';

function App() {
  return (
    <div className="App">
      <Home/>
    </div>
  );
}

export default App;
